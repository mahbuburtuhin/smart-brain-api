const bcrypt = require('bcrypt');
const registerHandler = (pg) => (req, res) => {
  try {
    const salt = bcrypt.genSaltSync(10);
    const { name, email, password } = req.body;
    const hash = bcrypt.hashSync(password,salt);
    pg.transaction(trx => {
      trx('login')
        .insert({
          email: email,
          hash: hash
        })
        .returning('email')
        .then(loginEmail => {
          return trx('users')
            .insert({
              name: name,
              email: loginEmail[0],
              joined: new Date()
              
            })
            .returning('*')
            .then(data => res.status(200).json(data[0]))
        })
        .then(trx.commit)
        .catch(trx.rollback);
    })
      .catch(err => {
        console.log(err);
        res.status(400).send('unable to register')
      });  
  } catch (err){
    console.log(err);
    res.json('unable to register');
  }
}

module.exports = registerHandler;
