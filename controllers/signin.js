const bcrypt = require('bcrypt');

const signinHandler = (pg) => (req, res) => {
  try {
    const { email, password } = req.body;
    pg('login')
      .select('*')
      .where({email})
      .then(data => {
        const vaild = bcrypt.compareSync(password, data[0].hash);
        if (vaild) {
          return pg('users')
            .select('*')
            .where({ email })
            .then(data => {
              res.status(200).json(data[0]);
            })     
        } else {
          res.status(401).json('wrong credentials')  
        }      
      })
      .catch(err => {
        console.log(err);
        res.status(400).json('wrong credentials')
    })
  } catch (err){
    console.log(err);
    res.json('unable to signin');
  }
  
}

module.exports = signinHandler;
