const express = require('express');
const cors = require('cors')

var pg = require('knex')({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'epic',
    password : '',
    database : 'smart-brain'
  }
});
const app = express();

app.use(cors());
app.use(express.json());

// handlers 
const singninHandler = require('./controllers/signin');
const registerHandler = require('./controllers/register');
const imageHandler = require('./controllers/image');
const profileHandler = require('./controllers/profile');

app.get('/', (req, res) => { pg('users').select('*').then(data => res.send(data)); });

app.post('/signin', singninHandler(pg));
app.post('/register', registerHandler(pg));
app.post('/image', imageHandler());
app.post('/profile/:id', profileHandler());

app.listen(3000);


// const db = [
//   {
//   name:'Jhon',
//   email:'jhon@email.com',
//   password:'123',
//   entries:0,
//   dateJoined: new Date()
//   },
//   {
//   name:'Mark',
//   email:'mark@email.com',
//   password:'abc',
//   entries:0,
//   dateJoined: new Date()
//   },
//   {
//   name:'Rahim',
//   email:'rahim@email.com',
//   password:'xyz',
//   entries:0,
//   dateJoined: new Date()
//   }
// ]